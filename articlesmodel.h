/**
 * SPDX-FileCopyrightText: 2019 Nicolas Fella <nicolas.fella@gmx.de>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#pragma once

#include <QAbstractListModel>
#include <QImage>
#include <QVariant>

struct Article {
    QString title;
    QString category;
    QString price;
    QString imageFile;
    bool available;
    QStringList declarations;
};

class ArticlesModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        TitleRole = Qt::DisplayRole,
        CategoryRole = Qt::UserRole + 1,
        PriceRole,
        ImageFileRole,
        AvailableRole,
        DeclarationsRole,
    };

    ArticlesModel(QObject *parent, bool useTestData);

    int rowCount(const QModelIndex &parent = {}) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

private:
    void loadData(const QByteArray &data);

    QVector<Article> m_articles;
    bool m_useTestData;
    QString m_urlBase = QStringLiteral("https://iceportal.de");
};
