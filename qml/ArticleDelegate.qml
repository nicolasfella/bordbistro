/**
 * SPDX-FileCopyrightText: 2019 Nicolas Fella <nicolas.fella@gmx.de>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11

import org.kde.kirigami 2.15 as Kirigami
import org.kde.kitemmodels 1.0 as Models

import bap 1.0

Column {
    id: root

    required property string title
    required property string price
    required property string imageFile
    required property bool available
    required property var declarations

    property bool expanded: false

    spacing: 2 * Kirigami.Units.largeSpacing

    Kirigami.BasicListItem {
        id: bli

        text: root.title
        subtitle: root.price
        enabled: root.available

        trailing: ToolButton {
            icon.name: root.expanded ? "go-down" : "go-next"
        }

        onClicked: root.expanded = !root.expanded
    }

    ColumnLayout {
        visible: expanded
        width: parent.width

        Image {
            id: img
            Layout.alignment: Qt.AlignHCenter
            source: root.imageFile
            fillMode: Image.Pad
        }

        Repeater {
            model: root.declarations

            delegate: Label {
                Layout.leftMargin: Kirigami.Units.largeSpacing
                text: "⚠ " + modelData
            }
        }

        Kirigami.Separator {
            Layout.fillWidth: true
            Layout.leftMargin: Kirigami.Units.largeSpacing
            Layout.rightMargin: Kirigami.Units.largeSpacing
        }
    }
}
