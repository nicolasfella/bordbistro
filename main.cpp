/**
 * SPDX-FileCopyrightText: 2019 Nicolas Fella <nicolas.fella@gmx.de>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#include <QApplication>
#include <QCommandLineParser>
#include <QQmlApplicationEngine>

#include "articlesmodel.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QCommandLineParser parser;
    parser.addOption(QCommandLineOption("testing"));

    parser.process(app);

    bool useTestData = parser.isSet("testing");

    QGuiApplication::setApplicationDisplayName("Bordbistro");

    QQmlApplicationEngine engine;

    ArticlesModel model(nullptr, useTestData);
    qmlRegisterSingletonInstance<ArticlesModel>("bap", 1, 0, "ArticlesModel", &model);

    engine.load("qrc:/qml/main.qml");

    return app.exec();
}
